﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using IGPSOSERV.Classes;

namespace IGPSOSERV
{
    public partial class IGPSOSERV : Form
    {
        public IGPSOSERV()
        {
            InitializeComponent();
        }

        Server loginServer;
        Server patchServer;
        Server shipServer;

        private void IGPSOSERV_Load(object sender, EventArgs e)
        {
            //Login server
            loginServer = new Server();
            loginServer.RemoteAddress = "127.0.0.1";
            loginServer.LocalPort = 9000;
            loginServer.RemotePort = 9000;
            loginServer.Start();

            //Patch server
            patchServer = new Server();
            patchServer.RemoteAddress = "127.0.0.1";
            patchServer.LocalPort = 9001;
            patchServer.RemotePort = 9001;
            patchServer.Start();

            //Ship server
            shipServer = new Server();
            shipServer.RemoteAddress = "127.0.0.1";
            shipServer.LocalPort = 9002;
            shipServer.RemotePort = 9002;
            shipServer.Start();

            //Loads players in checklist
            string[] myStringList = new string[] { "Player 1", "Player 2", "Player 3" }; //replace this with function that updates players
            foreach (string myString in myStringList)
            {
                connected_players.Items.Add(myString);
            }
        }

        private void kick_player_Click(object sender, EventArgs e)
        {
            foreach (object item in connected_players.CheckedItems)
            {
                //Do kick action to each checked item
                Admin adminFunction = new Admin();
                adminFunction.kick_player(item);
            }
        }

        private void reload_player_Click(object sender, EventArgs e)
        {
            foreach (object item in connected_players.CheckedItems)
            {
                //Do reload action to each checked item
                Admin adminFunction = new Admin();
                adminFunction.reload_player(item);
            }
        }

        private void ban_player_Click(object sender, EventArgs e)
        {
            foreach (object item in connected_players.CheckedItems)
            {
                //Do ban action to each checked item
                Admin adminFunction = new Admin();
                adminFunction.ban_player(item);
            }
        }
    }
}
