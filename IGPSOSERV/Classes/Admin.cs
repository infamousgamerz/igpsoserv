﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace IGPSOSERV.Classes
{
    class Admin
    {
        Server shipServer;

        //Globals
        public static class Globals
        {
            public static String config_dir = Directory.GetCurrentDirectory() + @"\config\";
        }

        //Kick player
        public void kick_player(object item)
        {
            byte[] kick_player_packet = { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };
            shipServer.m_vClient.SendToClient(kick_player_packet);
        }

        //Reload player
        public void reload_player(object item)
        {
            byte[] kick_player_packet = { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };
            shipServer.m_vClient.SendToClient(kick_player_packet);
        }

        //Ban player
        public void ban_player(object item)
        {
            string curFile1 = Globals.config_dir + "ban_list.txt";
            string result = Convert.ToString(item);
            File.AppendAllText(curFile1, result);
            File.AppendAllText(curFile1, "\r\n");
        }

        //Ban check
        public void ban_check()
        {
            string curFile1 = Globals.config_dir + "ban_list.txt";
            var lines = File.ReadAllLines(curFile1);
            using (StreamReader sr = new StreamReader(curFile1))
            {
                string contents = sr.ReadToEnd();
                foreach(var line in lines) {
                    if (contents.Contains(line))
                    {
                        //kick user here
                    }
                }
            }
        }
    }
}
