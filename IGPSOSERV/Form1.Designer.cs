﻿namespace IGPSOSERV
{
    partial class IGPSOSERV
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(IGPSOSERV));
            this.label1 = new System.Windows.Forms.Label();
            this.kick_player = new System.Windows.Forms.Button();
            this.reload_player = new System.Windows.Forms.Button();
            this.ban_player = new System.Windows.Forms.Button();
            this.connected_players = new System.Windows.Forms.CheckedListBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 2);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(126, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "CONNECTED PLAYERS";
            // 
            // kick_player
            // 
            this.kick_player.BackColor = System.Drawing.Color.White;
            this.kick_player.ForeColor = System.Drawing.Color.Black;
            this.kick_player.Location = new System.Drawing.Point(218, 19);
            this.kick_player.Name = "kick_player";
            this.kick_player.Size = new System.Drawing.Size(75, 23);
            this.kick_player.TabIndex = 2;
            this.kick_player.Text = "KICK";
            this.kick_player.UseVisualStyleBackColor = false;
            this.kick_player.Click += new System.EventHandler(this.kick_player_Click);
            // 
            // reload_player
            // 
            this.reload_player.BackColor = System.Drawing.Color.White;
            this.reload_player.ForeColor = System.Drawing.Color.Black;
            this.reload_player.Location = new System.Drawing.Point(218, 49);
            this.reload_player.Name = "reload_player";
            this.reload_player.Size = new System.Drawing.Size(75, 23);
            this.reload_player.TabIndex = 3;
            this.reload_player.Text = "RELOAD";
            this.reload_player.UseVisualStyleBackColor = false;
            this.reload_player.Click += new System.EventHandler(this.reload_player_Click);
            // 
            // ban_player
            // 
            this.ban_player.BackColor = System.Drawing.Color.White;
            this.ban_player.ForeColor = System.Drawing.Color.Black;
            this.ban_player.Location = new System.Drawing.Point(218, 79);
            this.ban_player.Name = "ban_player";
            this.ban_player.Size = new System.Drawing.Size(75, 23);
            this.ban_player.TabIndex = 4;
            this.ban_player.Text = "BAN";
            this.ban_player.UseVisualStyleBackColor = false;
            this.ban_player.Click += new System.EventHandler(this.ban_player_Click);
            // 
            // connected_players
            // 
            this.connected_players.FormattingEnabled = true;
            this.connected_players.Location = new System.Drawing.Point(6, 19);
            this.connected_players.Name = "connected_players";
            this.connected_players.Size = new System.Drawing.Size(206, 304);
            this.connected_players.TabIndex = 1;
            // 
            // IGPSOSERV
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.ClientSize = new System.Drawing.Size(298, 331);
            this.Controls.Add(this.ban_player);
            this.Controls.Add(this.reload_player);
            this.Controls.Add(this.kick_player);
            this.Controls.Add(this.connected_players);
            this.Controls.Add(this.label1);
            this.ForeColor = System.Drawing.Color.White;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "IGPSOSERV";
            this.Text = "IGPSOSERV";
            this.Load += new System.EventHandler(this.IGPSOSERV_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button kick_player;
        private System.Windows.Forms.Button reload_player;
        private System.Windows.Forms.Button ban_player;
        private System.Windows.Forms.CheckedListBox connected_players;
    }
}

